//
//  BallColor.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

enum BallColor: String, CaseIterable {
    case red
    case blue
    case green
    case yellow
    case white

    static var colors: [BallColor] {
        return [BallColor.red, .blue, .green, .yellow]
    }
    static var random: BallColor {
        return [BallColor.red, .blue, .green, .yellow].randomElement()!
    }
}
