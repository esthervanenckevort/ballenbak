//
//  BallGameType.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

protocol Game: class {
    func tapped(column: Int, row: Int)
    func saveGame() -> SavedGame
    init(game: SavedGame, coordinator: GameCoordinator, reporter: ScoreReporter)
    init(board size: (columns: Int, rows: Int), coordinator: GameCoordinator, reporter: ScoreReporter)
}
