//
//  Board.swift
//  Ballenbak
//
//  Created by David van Enckevort on 30-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

struct Board {
    internal let rows: Int
    internal let columns: Int
    private weak var presenter: GamePresenter!
    private (set) var board: [Ball?]
    typealias Cell = (column: Int, row: Int)
    typealias Size = (columns: Int, rows: Int)

    init(size: Size, presenter: GamePresenter) {
        self.columns = size.columns
        self.rows = size.rows
        self.presenter = presenter
        self.board = Array<Ball?>(repeating: nil, count: columns * rows)
    }

    subscript(cell: Cell) -> Ball? {
        get {
            return self.board[cell.column * rows + cell.row]
        }
        set {
            if let ball = newValue {
                ball.row = cell.row
                ball.column = cell.column
                self.board[cell.column * rows + cell.row] = ball
                presenter.update(ball: ball, column: cell.column, row: cell.row)
            }
            self.board[cell.column * rows + cell.row] = newValue
        }
    }

    private subscript(column column: Int) -> ArraySlice<Ball?> {
        let start = column * rows
        let end = (column + 1) * rows
        return board[start..<end]
    }

    func balls() -> [Ball] {
        return board.compactMap { $0 }
    }

    func cleared(color: BallColor) -> Bool {
        return balls().filter { $0.color == color } .isEmpty
    }

    func isEmpty(column: Int) -> Bool {
        return self[column: column].compactMap { $0 }.isEmpty
    }

    mutating func update() {
        moveEmptyCells()
        moveEmptyColumns()
    }

    private mutating func moveEmptyColumns() {
        for column in 0..<columns {
            if isEmpty(column: column) {
                for oldColumn in (0..<column).reversed() {
                    let newColumn = oldColumn + 1
                    for row in (0..<rows) {
                        swap(from: (column: oldColumn, row: row), to: (column: newColumn, row: row))
                    }
                }
            }
        }
    }

    private mutating func moveEmptyCells() {
        for column in 0..<columns {
            for row in 0..<rows {
                if self[(column: column, row: row)] != nil {
                    continue
                }
                for newRow in (row + 1)..<rows {
                    if self[(column: column, row: newRow)] == nil {
                        continue
                    }
                    swap(from: (column: column, row: row), to: (column: column, row: newRow))
                    break
                }
            }
        }
    }

    func neighbours(for cell: Cell) -> [Ball] {
        return [
            (cell.column + 1, cell.row),
            (cell.column - 1, cell.row),
            (cell.column, cell.row + 1),
            (cell.column, cell.row - 1)
            ].filter { $0.column >= 0
                    && $0.column < self.columns
                    && $0.row >= 0
                    && $0.row < self.rows
            }.compactMap { self[$0] }
    }

    private mutating func swap(from: (column: Int, row: Int), to: (column: Int, row: Int)) {
        let tmp = self[to]
        self[to] = self[from]
        self[from] = tmp
    }
}
