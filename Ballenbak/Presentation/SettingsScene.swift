//
//  SettingsScene.swift
//  Ballenbak
//
//  Created by David van Enckevort on 03-12-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation
import SpriteKit

final class SettingsScene: SKScene {
    weak var coordinator: GameCoordinator?
    private var recognizer: UITapGestureRecognizer?

    override func willMove(from view: SKView) {
        if let recognizer = recognizer {
            view.removeGestureRecognizer(recognizer)
        }
    }

    private func setUp() {
        if let musiclabel = childNode(withName: "backgroundmusic") as? SKLabelNode {
            musiclabel.text = UserDefaults.standard.useBackgroundMusic ? "🔊" : "🔇"
        }
        if let effectslabel = childNode(withName: "soundeffects") as? SKLabelNode {
            effectslabel.text = UserDefaults.standard.useSoundEffects ? "🔔" : "🔕"
        }
        if let highscoreslabel = childNode(withName: "gamecenter") as? SKLabelNode {
            highscoreslabel.text = UserDefaults.standard.useGameCenter ? "☑︎" : "☒"
        }
    }

    override func didMove(to view: SKView) {
        recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        view.addGestureRecognizer(recognizer!)
        setUp()
    }

    @objc func didTap(_ sender: UITapGestureRecognizer) {
        let point = convertPoint(fromView: sender.location(in: self.view))
        let node = atPoint(point)
        guard let name = node.name, let label = node as? SKLabelNode else { return }
        switch name {
        case "backgroundmusic":
            UserDefaults.standard.useBackgroundMusic.toggle()
            let text = UserDefaults.standard.useBackgroundMusic ? "🔊" : "🔇"
            animate(label, to: text)
        case "soundeffects":
            UserDefaults.standard.useSoundEffects.toggle()
            let text = UserDefaults.standard.useSoundEffects ? "🔔" : "🔕"
            animate(label, to: text)
        case "highscores":
            UserDefaults.standard.useGameCenter.toggle()
            let text = UserDefaults.standard.useGameCenter ? "☑︎" : "☒"
            animate(label, to: text)
        case "backbutton":
            coordinator?.presentMenu()
        default:
            break
        }
    }

    private func animate(_ button: SKLabelNode, to label: String) {
        let moveDown = SKAction.move(by: CGVector(dx: 4, dy: -4), duration: 0.05)
        let moveUp = SKAction.move(by: CGVector(dx: -4, dy: 4), duration: 0.05)
        let updateLabel = SKAction.run {
            button.text = label
        }
        var actions = [SKAction]()
        if UserDefaults.standard.useSoundEffects {
            let playSound = SKAction.playSoundFileNamed("switch", waitForCompletion: false)
            actions.append(playSound)
        }
        actions.append(contentsOf: [moveDown, updateLabel, moveUp])
        let sequence = SKAction.sequence(actions)
        button.run(sequence)
    }
}
