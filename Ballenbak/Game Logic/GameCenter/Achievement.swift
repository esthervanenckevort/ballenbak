//
//  Achievements.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation
enum Achievement: String {
    case clearedAllBlueBalls // 20 points
    case clearedAllGreenBalls // 20 points
    case clearedAllRedBalls // 20 points
    case clearedAllYellowBalls // 20 points
    case cleared1Column // 1 point
    case cleared2Columns // 1 point
    case cleared3Columns // 2 point
    case cleared4Columns // 3 point
    case cleared5Columns // 5 point
    case cleared6Columns // 8 points
    case cleared7Columns // 13 points
    case cleared8Columns // 21 points
    case cleared9Columns // 34 points
    case clearedBoard // 55 points
    case turn10points // 4 balls - 1 point
    case turn20points // 5 balls - 1 point
    case turn50points // 8 balls - 2 points
    case turn100points // 11 balls - 3 points
    case turn250points // 17 balls - 5 points
    case turn500points // 23 balls - 8 points
    case turn750points // 28 balls - 13 points
    case turn1000points // 33 balls - 21 points
    case game500points // 1 point
    case game750points // 1 points
    case game1000points // 2 points
    case game1500points  // 3 points
    case game2000points // 5 points
    case game2500points // 8 points
    case game3000points // 13 points
    case game4000points // 21 points
    case game5000points // 34 points

    static func awards(points: Int) -> Set<Achievement> {
        var awards = Set<Achievement>()
        switch points {
        case 1000...:
            awards.insert(.turn1000points)
            fallthrough
        case 750..<1000:
            awards.insert(.turn750points)
            fallthrough
        case 500..<750:
            awards.insert(.turn500points)
            fallthrough
        case 250..<500:
            awards.insert(.turn250points)
            fallthrough
        case 100..<250:
            awards.insert(.turn100points)
            fallthrough
        case 50..<100:
            awards.insert(.turn50points)
            fallthrough
        case 20..<50:
            awards.insert(.turn20points)
            fallthrough
        case 10..<20:
            awards.insert(.turn10points)
        default:
            break
        }
        return awards
    }

    static func awards(color: BallColor) -> Achievement {
        let award: [BallColor: Achievement] = [
            .blue: .clearedAllBlueBalls,
            .red: .clearedAllRedBalls,
            .green: .clearedAllGreenBalls,
            .yellow: .clearedAllYellowBalls
        ]
        return award[color]!
    }

    static func awards(columnsCleared: Int) -> Set<Achievement> {
        var awards = Set<Achievement>()
        switch columnsCleared {
        case 10:
            awards.insert(.clearedBoard)
            fallthrough
        case 9:
            awards.insert(.cleared9Columns)
            fallthrough
        case 8:
            awards.insert(.cleared8Columns)
            fallthrough
        case 7:
            awards.insert(.cleared7Columns)
            fallthrough
        case 6:
            awards.insert(.cleared6Columns)
            fallthrough
        case 5:
            awards.insert(.cleared5Columns)
            fallthrough
        case 4:
            awards.insert(.cleared4Columns)
            fallthrough
        case 3:
            awards.insert(.cleared3Columns)
            fallthrough
        case 2:
            awards.insert(.cleared2Columns)
            fallthrough
        case 1:
            awards.insert(.cleared1Column)
        default:
            break
        }
        return awards
    }

    static func awards(score: Int) -> Set<Achievement> {
        var awards = Set<Achievement>()
        switch score {
        case 5_000...:
            awards.insert(.game5000points)
            fallthrough
        case 4_000..<5_000:
            awards.insert(.game4000points)
            fallthrough
        case 3_000..<4_000:
            awards.insert(.game3000points)
            fallthrough
        case 2_500..<3_000:
            awards.insert(.game2500points)
            fallthrough
        case 2_000..<2_500:
            awards.insert(.game2000points)
            fallthrough
        case 1_500..<2_000:
            awards.insert(.game1500points)
            fallthrough
        case 1_000..<1_500:
            awards.insert(.game1000points)
            fallthrough
        case 750..<1_000:
            awards.insert(.game750points)
            fallthrough
        case 500..<750:
            awards.insert(.game500points)
        default:
            break
        }
        return awards
    }
}

