//
//  ScoreReporter.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

protocol ScoreReporter: class {
    func report(achievements: Set<Achievement>)
    func report(board: LeaderBoard, score: Int)
}
