//
//  MenuScene.swift
//  Ballenbak
//
//  Created by David van Enckevort on 03-12-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit

final class MenuScene: SKScene {

    weak var gameCoordinator: GameCoordinator?
    private var recognizer: UITapGestureRecognizer?

    override func didMove(to view: SKView) {
        recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        view.addGestureRecognizer(recognizer!)
        for name in BallColor.colors.map({ $0.rawValue }) {
            let emitter = SKEmitterNode()
            emitter.particleBirthRate = 2.4
            emitter.particleTexture = SKTexture(imageNamed: name)
            emitter.particlePositionRange = CGVector(dx: size.width * 2.0, dy: 10)
            emitter.particleScale = 0.30
            emitter.particleScaleRange = 0.20
            emitter.xAcceleration = 0.0
            emitter.yAcceleration = -10.0
            emitter.emissionAngle = 270
            emitter.emissionAngleRange = 25
            emitter.particleLifetime = 20
            emitter.particleAlpha = 0.65
            emitter.particleAlphaRange = 0.20
            emitter.particleSpeed = 80
            emitter.particleSpeedRange = 100
            emitter.position = CGPoint(x: 375, y: 1334)
            emitter.zPosition = -50
            emitter.advanceSimulationTime(10)
            addChild(emitter)
        }
    }

    override func willMove(from view: SKView) {
        if let recognizer = recognizer {
            view.removeGestureRecognizer(recognizer)
        }
    }

    @objc func didTap(_ sender: UITapGestureRecognizer) {
        let position = self.convertPoint(fromView: sender.location(in: self.view))
        let node = self.atPoint(position)
        guard let name = node.name else { return }
        switch name {
        case "play":
            gameCoordinator?.startGame()
        case "tutorial":
            gameCoordinator?.startTutorial()
        case "settings":
            gameCoordinator?.presentSettings()
        case "gamecenter":
            gameCoordinator?.presentScoreBoard()
        default:
            break
        }

    }
}
