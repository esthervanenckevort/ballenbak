//
//  GameState.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

enum GameState {
    case playing(score: Int, ballsLeft: Int, columnsCleared: Int)
    case gameover(score: Int, ballsLeft: Int, columnsCleared: Int)
}
