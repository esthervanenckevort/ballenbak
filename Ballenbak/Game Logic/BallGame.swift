//
//  BallGame.swift
//  Ballenbak
//
//  Created by David van Enckevort on 27-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

final class BallGame: Game {

    func saveGame() -> SavedGame {
        let savedBoard = board.board.map {$0 == nil ? "nil" : $0!.color.rawValue}
        return SavedGame(score: score, achievements: awards.map {$0.rawValue}, board: savedBoard, columns: board.columns, rows: board.rows)
    }

    required init(game: SavedGame, coordinator: GameCoordinator, reporter: ScoreReporter) {
        board = Board(size: (columns: game.columns, rows: game.rows), presenter: coordinator.presenter)
        score = game.score
        self.coordinator = coordinator
        self.reporter = reporter
        selectedBalls = Set<Ball>()
        awards = Set(game.achievements.map { Achievement(rawValue: $0) }.compactMap {$0})
        for column in 0..<board.columns {
            for row in 0..<board.rows {
                guard let color = BallColor(rawValue: game.board[column * board.rows + row]) else { continue }
                self.board[(column: column, row: row)] = Ball(color: color, column: column, row: row)
            }
        }
    }

    required init(board size: (columns: Int, rows: Int), coordinator: GameCoordinator, reporter: ScoreReporter) {
        self.board = Board(size: size, presenter: coordinator.presenter)
        self.score = 0
        self.coordinator = coordinator
        self.reporter = reporter
        selectedBalls = Set<Ball>()
        self.awards = Set<Achievement>()
        for column in 0..<board.columns {
            for row in 0..<board.rows {
                board[(column: column, row: row)] = Ball(color: BallColor.random, column: column, row: row)
            }
        }
    }

    private func updateAwards(pointsEarned: Int, columnsCleared: Int) {
        let colorAwards = BallColor.colors
            .compactMap { board.cleared(color: $0) ? Achievement.awards(color: $0) : nil }
        awards.formUnion(Achievement.awards(points: pointsEarned))
        awards.formUnion(Achievement.awards(score: score))
        awards.formUnion(Achievement.awards(columnsCleared: columnsCleared))
        awards.formUnion(Set(colorAwards))
    }

    private func calculateColumnsCleared() -> Int {
        var columnsCleared = 0
        for column in 0..<board.columns {
            if board.isEmpty(column: column) {
                columnsCleared += 1
            }
        }
        return columnsCleared
    }

    private func calculateAwards() {
        let pointsEarned = getPoints(balls: selectedBalls.count)
        score += pointsEarned

        if pointsEarned > 100 {
            if let ball = (board.board.compactMap { return $0 }.randomElement()) {
                ball.color = .white
                coordinator.presenter.update(ball: ball, column: ball.column, row: ball.row)
            }
        }
        let ballsLeft = board.balls().count
        let columnsCleared = calculateColumnsCleared()
        updateAwards(pointsEarned: pointsEarned, columnsCleared: columnsCleared)
        if hasMoreMoves() {
            coordinator.presenter.update(state: .playing(score: score, ballsLeft: ballsLeft, columnsCleared: columnsCleared))
        } else {
            reporter.report(board: .points, score: score)
            reporter.report(board: .columnsCleared, score: columnsCleared)
            reporter.report(board: .ballsLeft, score: ballsLeft)
            coordinator.presenter.update(state: .gameover(score: score, ballsLeft: ballsLeft, columnsCleared: columnsCleared))
        }
    }

    internal func tapped(column: Int, row: Int) {
        guard column >= 0, column < board.columns, row >= 0, row < board.rows else {
            selectedBalls.removeAll()
            return
        }
        let ball = board[(column: column, row: row)]
        if let ball = ball, selectedBalls.contains(ball) {
            removeBalls()
            calculateAwards()
            selectedBalls.removeAll()
        } else {
            selectedBalls = selectBalls(ball: ball)
        }
    }

    private var board: Board
    private unowned var coordinator: GameCoordinator
    private var reporter: ScoreReporter

    private var selectedBalls: Set<Ball> {
        didSet {
            coordinator.presenter.deselect(balls: oldValue.subtracting(selectedBalls))
            coordinator.presenter.select(balls: selectedBalls.subtracting(oldValue))
            if !selectedBalls.isEmpty {
                coordinator.presenter.show(score: getPoints(balls: selectedBalls.count))
            }
        }
    }
    private var score: Int
    private var awards: Set<Achievement> {
        didSet {
            reporter.report(achievements: awards.subtracting(oldValue))
        }
    }

    private func findNeighbours(for ball: Ball, color: BallColor, ignoring seen: Set<Ball>) -> Set<Ball> {
        var seen = seen
        seen.insert(ball)
        for neighbour in board.neighbours(for: (column: ball.column, row: ball.row)) {
            if (neighbour.color == color || neighbour.color == .white) && !seen.contains(neighbour) {
                seen.formUnion(findNeighbours(for: neighbour, color: color, ignoring: seen))
            } else {
                continue
            }
        }
        return seen
    }

    private func removeBalls() {
        let balls = selectedBalls
        for ball in balls {
            board[(column: ball.column, row: ball.row)] = nil
        }
        coordinator.presenter.remove(balls: balls)
        board.update()
    }

    private func hasMoreMoves() -> Bool {
        let validMovesCount = board.balls()
            .map { findNeighbours(for: $0, color: $0.color, ignoring: Set<Ball>()).count }
            .filter { $0 > 2 }
            .count
        return validMovesCount > 0
    }

    private func selectBalls(ball: Ball?) -> Set<Ball> {
        guard let ball = ball else { return Set<Ball>() }
        var balls = findNeighbours(for: ball, color: ball.color, ignoring: Set<Ball>())
        if balls.count < 3 {
            balls.removeAll()
        }
        return balls
    }

    private func getPoints(balls: Int) -> Int {
        return balls * balls - balls
    }
}
