//
//  GameScene.swift
//  Ballenbak
//
//  Created by David van Enckevort on 21-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    private weak var coordinator: GameCoordinator!
    private var columns: CGFloat
    private var rows: CGFloat
    private var diameter: CGFloat { return frame.width / columns }
    private var recognizer: UITapGestureRecognizer?
    private var balls: [SpriteKitBall] {
        return children.compactMap { $0 as? SpriteKitBall }
    }

    init(board: (columns: Int, rows: Int), coordinator: GameCoordinator) {
        self.coordinator = coordinator
        columns = CGFloat(board.columns)
        rows = CGFloat(board.rows)
        super.init(size: CGSize(width: 1000.0, height: 1600.0))
        scaleMode = .aspectFit
        backgroundColor = .black
    }

    required init?(coder aDecoder: NSCoder) {
        columns = CGFloat(aDecoder.decodeFloat(forKey: "columns"))
        rows = CGFloat(aDecoder.decodeFloat(forKey: "rows"))
        super.init(coder: aDecoder)
    }

    override func encode(with aCoder: NSCoder) {
        aCoder.encode(Float(columns), forKey: "columns")
        aCoder.encode(Float(rows), forKey: "rows")
    }

    override func didMove(to view: SKView) {
        recognizer = UITapGestureRecognizer(target: self, action: #selector(GameScene.didTap(_:)))
        view.addGestureRecognizer(recognizer!)
    }

    override func willMove(from view: SKView) {
        if let recognizer = recognizer {
            view.removeGestureRecognizer(recognizer)
        }
    }

    fileprivate func isInBounds(point: CGPoint) -> Bool {
        return point.x >= 0 && point.x < size.width &&
            point.y >= 0 && point.y < size.height
    }

    @objc func didTap(_ sender: UITapGestureRecognizer) {
        let point = convertPoint(fromView: sender.location(in: self.view))
        if let name = atPoint(point).name, name == "gameover" {
            coordinator?.presentMenu()
        } else if isInBounds(point: point) {
            let column = Int((point.x) / diameter)
            let row = Int(point.y / diameter)
            hideScore()
            coordinator.game.tapped(column: column, row: row)
        }
    }

    private func getNode(for ball: Ball) -> SpriteKitBall {
        if let node = balls.filter({ $0.ball == ball }).first {
            return node
        } else {
            let node = SpriteKitBall(imageNamed: ball.color.rawValue)
            node.name = ball.color.rawValue
            node.ball = ball
            node.anchorPoint = .zero
            return node
        }
    }

    private func playSound(named name: String) {
        if UserDefaults.standard.useSoundEffects {
            scene?.run(SKAction.playSoundFileNamed(name, waitForCompletion: false))
        }
    }
}

extension GameScene: GamePresenter {

    func award(achievement: Achievement) {
    }
    
    func update(left: Int) {
        let total = Int(rows * columns)
        var label: SKLabelNode? = childNode(withName: "balls") as? SKLabelNode
        if label == nil {
            label = SKLabelNode(text: "\(left)/\(total)")
            label?.name = "balls"
            label?.fontSize = 64
            label?.fontName = "Avenir-Black"
            label?.horizontalAlignmentMode = .right
            label?.verticalAlignmentMode = .top
            let position = CGPoint(x: frame.width - 8, y: frame.height - 8)
            label?.position = position
            label?.zPosition = 100
            addChild(label!)
        } else {
            label?.text = "\(left)/\(total)"
        }
    }

    func update(score: Int) {
        var label: SKLabelNode? = childNode(withName: "score") as? SKLabelNode
        if label == nil {
            label = SKLabelNode(text: "\(score)")
            label?.name = "score"
            label?.fontSize = 64
            label?.fontName = "Avenir-Black"
            label?.horizontalAlignmentMode = .left
            label?.verticalAlignmentMode = .top
            let position = CGPoint(x: 8, y: frame.height - 8)
            label?.position = position
            label?.zPosition = 100
            addChild(label!)
        } else {
            label?.text = "\(score)"
        }
    }

    func update(state: GameState) {
        switch state {
        case .playing(let score, let ballsLeft, _):
            update(score: score)
            update(left: ballsLeft)
        case .gameover(let score, let ballsLeft, _):
            update(score: score)
            update(left: ballsLeft)
            let position = CGPoint(x: frame.width / 2, y: frame.height / 2)
            let label = SKLabelNode(text: "Game Over!")
            label.fontSize = 128
            label.fontName = "Avenir-Black"
            label.fontColor = .white
            label.name = "gameover"
            label.horizontalAlignmentMode = .center
            label.verticalAlignmentMode = .center
            label.position = position
            addChild(label)
        }
    }

    func remove(balls: Set<Ball>) {
        if balls.count > 0 {
            playSound(named: "switch")
        }
        for ball in balls {
            let node = getNode(for: ball)
            node.removeFromParent()
        }
    }

    func update(ball: Ball, column: Int, row: Int) {
        let node = getNode(for: ball)
        var actions = [SKAction]()
        if ball.color == .white {
            let pulsate = SKAction.sequence([
                SKAction.fadeIn(withDuration: 0.3),
                SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.1),
                SKAction.colorize(with: .blue, colorBlendFactor: 1, duration: 0.1),
                SKAction.colorize(with: .green, colorBlendFactor: 1, duration: 0.1),
                SKAction.colorize(with: .yellow, colorBlendFactor: 1, duration: 0.1),
                SKAction.fadeOut(withDuration: 0.3),
                ])
            actions.append(SKAction.repeatForever(pulsate))
        }
        let destination = CGPoint(x: CGFloat(column) * diameter, y: CGFloat(row) * diameter)
        node.size = CGSize(width: diameter, height: diameter)
        actions.append(SKAction.move(to: destination, duration: 0.2))
        node.removeAllActions()
        node.run(SKAction.group(actions))
        if node.parent == nil {
            addChild(node)
        }
    }

    func select(balls: Set<Ball>) {
        if balls.count > 0 {
            playSound(named: "switch")
        }
        balls.forEach { ball in
            let node = getNode(for: ball)
            node.colorBlendFactor = 0.4
            node.color = .black
        }
    }

    func deselect(balls: Set<Ball>) {
        if balls.count > 0 {
            playSound(named: "switch")
        }
        balls.forEach { ball in
            let node = getNode(for: ball)
            node.colorBlendFactor = 0
        }
    }

    func show(score: Int) {
        let position = CGPoint(x: frame.width / 2, y: frame.height / 2)
        let label = SKLabelNode(text: "\(score)")
        label.fontSize = 128
        label.fontName = "Avenir-Black"
        label.name = "selected-score"
        label.horizontalAlignmentMode = .center
        label.verticalAlignmentMode = .center
        label.position = position
        addChild(label)
    }

    func hideScore() {
        childNode(withName: "selected-score")?.removeFromParent()
    }
}

