//
//  BallenbakTests.swift
//  BallenbakTests
//
//  Created by David van Enckevort on 21-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import XCTest
@testable import Ballenbak

class BallenbakTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func fibonacci(value: Int) -> Int {
        guard value > 1 else { return value > 0 ? value : 0 }
        return fibonacci(value: value - 1) + fibonacci(value: value - 2)
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let results: [Int] = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181]
        for value in 0..<results.count {
            let result = fibonacci(value: value)
            XCTAssert(result == results[value], "value: \(value) expected \(results[value]) actual \(result)")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
