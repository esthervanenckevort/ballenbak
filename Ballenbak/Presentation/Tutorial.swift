//
//  Tutorial.swift
//  Ballenbak
//
//  Created by David van Enckevort on 02-12-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation
import SpriteKit

final class Tutorial: GameScene {
    let tutorial = [
        "Tap any ball to start the tutorial",
        "Three or more adjectant balls of the same color can be removed.",
        "The goal of the game is to remove as many balls as possible.",
        "Removing more balls will increase your score.",
        "A preview of the score is given on the first tap.",
        "A second tap on one of the selected balls will remove them.",
        "Tapping anywhere else will deselect the balls.",
        "You can earn achievements for exceptional scores",]
    var hint = 0

    override func didMove(to view: SKView) {
        super.didMove(to: view)
        showHint(text: tutorial[hint])
        hint += 1
    }

    override func willMove(from view: SKView) {
        super.willMove(from: view)
    }

    override func didTap(_ sender: UITapGestureRecognizer) {
        childNode(withName: "tutorial")?.removeFromParent()
        if hint < tutorial.count {
            showHint(text: tutorial[hint])
            hint += 1
        }
        super.didTap(sender)
    }

    private func showHint(text: String) {
        let label = SKLabelNode(text: text)
        label.name = "tutorial"
        label.numberOfLines = 0
        label.preferredMaxLayoutWidth = size.width - 20
        label.fontSize = 48
        label.fontName = "Avenir-Black"
        label.zPosition = 100
        label.horizontalAlignmentMode = .center
        label.verticalAlignmentMode = .top
        label.position = CGPoint(x: size.width / 2, y: size.height - 70)
        addChild(label)
    }
}
