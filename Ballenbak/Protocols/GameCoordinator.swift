//
//  ScoreBoardPresenter.swift
//  Ballenbak
//
//  Created by David van Enckevort on 03-12-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

protocol GameCoordinator: class {
    var presenter: GamePresenter { get }
    var game: Game { get }
    
    func presentScoreBoard()
    func startGame()
    func startTutorial()
    func presentMenu()
    func presentSettings()
}
