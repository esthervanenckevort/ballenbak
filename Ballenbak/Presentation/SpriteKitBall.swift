//
//  SpriteKitBall.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import SpriteKit

final class SpriteKitBall: SKSpriteNode {
    var ball: Ball?
}
