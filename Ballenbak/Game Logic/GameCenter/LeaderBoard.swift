//
//  Leaderboard.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

enum LeaderBoard: String {
    case points
    case columnsCleared
    case ballsLeft
}
