//
//  Ball.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

final class Ball: Hashable {
    var hashValue: Int {
        return id
    }
    var color: BallColor
    let id: Int
    var column: Int
    var row: Int

    init(color: BallColor, column: Int, row: Int) {
        self.color = color
        self.id = Int.random(in: Int.min...Int.max)
        self.column = column
        self.row = row
    }
    
    static func ==(lhs: Ball, rhs: Ball) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }

}
