//
//  Settings.swift
//  Ballenbak
//
//  Created by David van Enckevort on 03-12-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

extension UserDefaults {
    private enum Keys: String {
        case useGameCenter, useBackgroundMusic, useSoundEffects
    }
    @objc dynamic var useBackgroundMusic: Bool {
        get {
            return bool(forKey: Keys.useBackgroundMusic.rawValue)
        }
        set {
            setValue(newValue, forKey: Keys.useBackgroundMusic.rawValue)
        }
    }
    @objc dynamic var useSoundEffects: Bool {
        get {
            return bool(forKey: Keys.useSoundEffects.rawValue)
        }
        set {
            setValue(newValue, forKey: Keys.useSoundEffects.rawValue)
        }
    }
    @objc dynamic var useGameCenter: Bool {
        get {
            return bool(forKey: Keys.useGameCenter.rawValue)
        }
        set {
            setValue(newValue, forKey: Keys.useGameCenter.rawValue)
        }
    }
}
