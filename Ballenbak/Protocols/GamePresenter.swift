//
//  BallGamePresenter.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

protocol GamePresenter: class {
    func remove(balls: Set<Ball>)
    func update(ball: Ball, column: Int, row: Int)
    func select(balls: Set<Ball>)
    func deselect(balls: Set<Ball>)
    func show(score: Int)
    func update(state: GameState)
    func award(achievement: Achievement)
}
