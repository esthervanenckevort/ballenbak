//
//  GameCenterScoreReporter.swift
//  Ballenbak
//
//  Created by David van Enckevort on 29-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import GameKit

final class GameCenterScoreReporter: ScoreReporter {
    func report(achievements: Set<Achievement>) {
        guard GKLocalPlayer.local.isAuthenticated else { return }
        var awards = [GKAchievement]()
        if GKLocalPlayer.local.isAuthenticated {
            for achievement in achievements {
                let award = GKAchievement(identifier: achievement.rawValue)
                award.percentComplete = 100.0
                award.showsCompletionBanner = true
                awards.append(award)
            }
        }
        GKAchievement.report(awards, withCompletionHandler: nil)
    }

    func report(board: LeaderBoard, score: Int) {
        guard GKLocalPlayer.local.isAuthenticated else { return }
        let points = GKScore(leaderboardIdentifier: board.rawValue)
        points.value = Int64(score)
        GKScore.report([points], withCompletionHandler: nil)
    }
}
