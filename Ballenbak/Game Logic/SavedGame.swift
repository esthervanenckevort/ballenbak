//
//  SavedGame.swift
//  Ballenbak
//
//  Created by David van Enckevort on 02-12-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

struct SavedGame: Codable {
    let score: Int
    let achievements: [String]
    let board: [String]
    let columns: Int
    let rows: Int
}
