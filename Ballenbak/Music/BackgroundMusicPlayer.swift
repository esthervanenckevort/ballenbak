//
//  BackgroundMusicPlayer.swift
//  Ballenbak
//
//  Created by David van Enckevort on 17-09-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import Foundation
import AVFoundation

class BackgroundMusicPlayer: NSObject {
    private let player: AVAudioPlayer
    private var observer: NSKeyValueObservation!

    override init(){
        guard let url = Bundle.main.url(forResource: "Crystal-Caverns", withExtension: "mp3") else {
            fatalError("Music not found!")
        }
        player = try! AVAudioPlayer(contentsOf: url)
        player.numberOfLoops = -1
        super.init()

        if UserDefaults.standard.useBackgroundMusic {
            player.play()
        }
        observer = UserDefaults.standard.observe(\.useBackgroundMusic, options: [.new]) {
            [unowned self] (defaults, change) in
            guard let shouldPlay = change.newValue else { return }
            if shouldPlay {
                self.player.play()
            } else {
                self.player.pause()
            }
        }
    }

    deinit {
        observer?.invalidate()
    }
}
