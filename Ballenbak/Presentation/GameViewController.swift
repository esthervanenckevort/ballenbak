//
//  GameViewController.swift
//  Ballenbak
//
//  Created by David van Enckevort on 21-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit

final class GameViewController: UIViewController {
    private var ballGame: Game?
    private var scene: GameScene?
    private var sceneView: SKView! {
        return view as? SKView
    }
    private var transition: SKTransition?
    let board = (columns: 10, rows: 15)

    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.useGameCenter {
            authenticateLocalPlayer()
        }
        guard let view = view as? SKView else { return }
        view.showsFPS = true
        view.showsNodeCount = true
        presentMenu()
    }
}

extension GameViewController: GameCoordinator {
    var presenter: GamePresenter {
        return scene!
    }

    var game: Game {
        return ballGame!
    }

    func presentSettings() {
        guard let scene = SKScene(fileNamed: "SettingsScene") as? SettingsScene else {
            fatalError("Failed to initialize settings scene")
        }
        scene.coordinator = self
        scene.scaleMode = .aspectFill
        transition = SKTransition.flipVertical(withDuration: 0.5)
        sceneView.presentScene(scene, transition: transition!)
    }

    func presentMenu() {
        guard let scene = SKScene(fileNamed: "MenuScene") as? MenuScene else {
            fatalError("Failed to initialize menu scene")
        }
        scene.gameCoordinator = self
        scene.scaleMode = .aspectFit
        if transition == nil {
            transition = SKTransition.fade(withDuration: 0.5)
        }
        sceneView?.presentScene(scene, transition: transition!)
    }

    private func start(scene: GameScene, game: Game) {
        self.scene = scene
        self.ballGame = game
        transition = SKTransition.fade(withDuration: 0.5)
        sceneView.presentScene(scene, transition: transition!)
    }

    func startGame() {
        scene = GameScene(board: board, coordinator: self)
        start(scene: scene!, game:  BallGame(board: board, coordinator: self, reporter: GameCenterScoreReporter()))
    }

    func startTutorial() {
        guard let tutorial = loadTutorialData() else {
            fatalError("Couldn't load tutorial data.")
        }
        scene = Tutorial(board: board, coordinator: self)
        start(scene: scene!, game: BallGame(game: tutorial, coordinator: self, reporter: GameCenterScoreReporter()))
    }

    private func loadTutorialData() -> SavedGame? {
        guard let url = Bundle.main.url(forResource: "tutorial", withExtension: "json"),
            let tutorial = try? Data(contentsOf: url) else {
                return nil
        }
        let decoder = JSONDecoder()
        return try? decoder.decode(SavedGame.self, from: tutorial)
    }

    func presentScoreBoard() {
        if UserDefaults.standard.useGameCenter {
            let gc = GKGameCenterViewController()
            gc.gameCenterDelegate = self
            present(gc, animated: true, completion: nil)
        }
    }
}

extension GameViewController: GKGameCenterControllerDelegate {

    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        dismiss(animated: true, completion: nil)
    }

    func authenticateLocalPlayer() {
        let localPlayer = GKLocalPlayer.local
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            guard let vc = viewController else {
                return
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
}
